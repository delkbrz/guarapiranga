======================================================================
███████╗██╗  ██╗██╗   ██╗    ██╗  ██╗ █████╗ ██╗    ██╗██╗  ██╗███████╗
██╔════╝██║ ██╔╝╚██╗ ██╔╝    ██║  ██║██╔══██╗██║    ██║██║ ██╔╝██╔════╝
███████╗█████╔╝  ╚████╔╝     ███████║███████║██║ █╗ ██║█████╔╝ ███████╗
╚════██║██╔═██╗   ╚██╔╝      ██╔══██║██╔══██║██║███╗██║██╔═██╗ ╚════██║
███████║██║  ██╗   ██║       ██║  ██║██║  ██║╚███╔███╔╝██║  ██╗███████║
╚══════╝╚═╝  ╚═╝   ╚═╝       ╚═╝  ╚═╝╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚══════╝
                        ┌─┐┌┬┐┬ ┬┌┬┐┬┌─┐┌─┐
                        └─┐ │ │ │ ││││ │└─┐
                        └─┘ ┴ └─┘─┴┘┴└─┘└─┘
=======================================================================

Conceito Inicial

Jogo de terror em primeira pessoa, baseado no caso “Guarapiranga”
Caso Guarapiranga

- O caso foi mostrado pela primeira vez em 1993 pela revista UFO, edição 25
- Na matéria falava de um homem que havia sido encontrado com estranhas marcas de mutilação em seu corpo, marcas semelhantes àquelas que são encontradas em casos de mutilação de animais silvestres.
- A matéria foi publicada por Encarnación Zapata Garcia, uma ufóloga espanhola reconhecida por uma parte da comunidade de ufologia internacional, mas chamada de maluca por outra parte.
- Zapata tomou conhecimento do caso a partir de um conceituado médico paulada, o dermatologista Rubens Góes, que mostrou fotografias enviadas por seu primo, Rubens Silvestre, perito criminal do Governo de São Paulo, do corpo do homem estranhamente mutilado
- Um documento revelou que o cadáver foi encontrado em 29 de setembro de 1988, vestindo apenas uma cueca. 
- A vítima teria 50 anos de idade e diversas marcas de outras mordidas, como de urubus que estavam comendo o corpo em decomposição próximo da represa
- As imagens que a pesquisadora conseguiu do corpo mostram incisões e ferimentos que batem com outros casos de possíveis chupa cabras no mundo
- O corpo foi encontrado sem os olhos, orelhas, lábios, o saco escrotal, sem o ânus e as vísceras. O corpo apresentava algumas perfurações nos ombros, pés, na coxa esquerda e abdômen.
- Para o Instituto Nacional de Investigação de Fenômenos Aeroespaciais (INFA) e o Instituto Nacional de Pesquisas Ufológicas (INPU), se tratava da ação de predadores muito bem conhecidos, tais como cachorros domésticos, do mato, jaguatiricas, onças etc.
- A vítima, Joaquim Sebastião Gonçalves, sofria do mal de Chagas e de epilepsia. Ele tomava o medicamento Gardenal. Lamentavelmente bebia muito.
- Ele não morava próximo do local onde foi encontrado, mas sempre ia lá pescar. Estava desaparecido havia três dias. Ele chegou em um dos braços da Represa Billings, no Jardim Recanto do Sol, Bairro Bororó, São Paulo, tirou a roupa, ficando só de cueca, e a colocou em uma maleta, que escondeu no meio da mata. Atravessou a represa, que tem uns 80 m, e foi pescar do outro lado
- O médico legista constatou que a vítima morreu cerca de 24 horas antes de ter sido encontrada. Os laudos atestam que o cadáver já estava em processo de decomposição.
- Os laudos oficiais foram ocultados e o caso arquivado por todas as autoridades que participaram
- Especialistas que analisam o caso hoje em dia dizem que a vítima teve uma crise epilética e como estava longe das pessoas não foi socorrido a tempo, abrindo oportunidade para animais silvestres atacarem o corpo do homem inconsciente
- A comunidade ufóloga diz que o caso não é tão simples assim, já que os ferimentos são profundos e sem padrões

Para imagens do caso e mais informações: 
http://www.fenomenum.com.br/ufo/casuistica/1980/guarapiranga
Local
Represa do Guarapiranga e imediações, São Paulo, SP
Informações relevantes

Plataforma: PC
Gênero: Terror,Suspense
Plot Inicial: “Caso Guarapiranga” “Chupa Cabra do Guarapiranga”
Inspirações inciais: Slender, Rake

Discord Oficial: https://discordapp.com/invite/8jUJZ9k


