﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pacifico : MonoBehaviour
{
    float campo;
    public float campodevisao = 50;
    public float velocidade = 6;

    void Update()
    {
        FindClosestEnemy();       
    }
    void fugir ()
    {
        transform.Translate(Vector3.forward * velocidade * Time.deltaTime);
    }
    //------------------------------------------//
    Hostil closestEnemy = null;
    void FindClosestEnemy()
	{
        //Localiza o Inimigo mais proximo
		float distanceToClosestEnemy = Mathf.Infinity;
		Hostil[] allEnemies = GameObject.FindObjectsOfType<Hostil>();

		foreach (Hostil currentEnemy in allEnemies) {
			float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
			if (distanceToEnemy < distanceToClosestEnemy) {
				distanceToClosestEnemy = distanceToEnemy;
				closestEnemy = currentEnemy;
			}
		}
        //Desenha uma linha entre os objetos no Debug
		Debug.DrawLine (this.transform.position, closestEnemy.transform.position);
        //Olhar para o Inimigo
        Quaternion visao;
        visao = Quaternion.LookRotation(transform.position - closestEnemy.transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, visao, 6f * Time.deltaTime);
        campo = Vector3.Distance(transform.position, closestEnemy.transform.position);

        
        if (campo <= campodevisao)
        {
        fugir();
        }

	}
}
