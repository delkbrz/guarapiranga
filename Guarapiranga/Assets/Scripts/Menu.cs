﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public string newGameLevel;
    public void NovoJogoBotao(string newGameLevel)
    {
            
        int y = SceneManager.GetActiveScene().buildIndex;
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(newGameLevel);
        SceneManager.UnloadSceneAsync(y);
    }

          
    public void SairBotao()
    {
        Application.Quit();
    }
}
